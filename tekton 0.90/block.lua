require ("middleclass")
local physics = require "physics"

block = class('block')

function block:initalize()

	self:clear()
end

function block:clear()
	self.id                        = nil
	self.color                     = nil
	self.imageSheetOutside         = nil
	self.imageOutside              = nil
	self.optionsImageSheetOutside  = nil
	self.imageSheetInside          = nil
	self.imageInside               = nil	
	self.optionsImageSheetInside   = nil
	--self.displayGroup              = nil
end

function block:init( iXPosition, iYPosition, iLength, strColorInside, strColorOutside )
	self.id = "block class"
	self.color = strColorInside
	--self.displayGroup = display.newGroup()
	--print(iXPosition, iYPosition, iLength, strColorInside, strColorOutside)
	
	----------------------------------------------------	
	local intOutsideTexturePosition_x = math.random(1, (512-iLength) )
	local intOutsideTexturePosition_y = math.random(1, (512-iLength) )
	
	self.optionsImageSheetOutside =
	{
		frames = {
			-- FRAME 1:
			{
	            x = intOutsideTexturePosition_x,
	            y = intOutsideTexturePosition_y,
	            width  = iLength,
	            height = iLength
	        },
		},
		
		sheetContentWidth  = 512,
		sheetContentHeight = 512
	}
	
	self.imageSheetOutside = graphics.newImageSheet( "images/color_"..strColorOutside..".jpg",
													 self.optionsImageSheetOutside )
	self.imageOutside      = display.newImage( self.imageSheetOutside, 1 )
	self.imageOutside.id     = "block"
	self.imageOutside.aBlock = self
	
	self.imageOutside:setStrokeColor( 0, 0, 0, 90 )
	self.imageOutside.strokeWidth = 2
	----------------------------------------------------
	local intInsideTexturePosition_x = math.random(1, (512-iLength) )
	local intInsideTexturePosition_y = math.random(1, (512-iLength) )
	
	local optionsImageSheetInside =
	{
		frames = {
			-- FRAME 1:
			{
	            x = intInsideTexturePosition_x,
	            y = intInsideTexturePosition_y,
	            width  = iLength*.60,
	            height = iLength*.60
	        },
		},
		
		sheetContentWidth  = 512,
		sheetContentHeight = 512
	}
	
	self.optionsImageSheetInside = optionsImageSheetInside
	
	self.imageSheetInside = graphics.newImageSheet( "images/color_"..strColorInside..".jpg", self.optionsImageSheetInside )
	self.imageInside      = display.newImage( self.imageSheetInside, 1 )	
	self.imageInside:setStrokeColor( 0, 0, 0, 80 )
	self.imageInside.strokeWidth = 4
	----------------------------------------------------
	
	self.imageOutside.x = iXPosition
	self.imageOutside.y = iYPosition
	self.imageInside.x  = iXPosition
	self.imageInside.y  = iYPosition
	--self.displayGroup.x = iXPosition
	--self.displayGroup.y = iYPosition
	
	--self.displayGroup:insert( self.imageOutside )
	--self.displayGroup:insert( self.imageInside )
	
   --physics.addBody( self.displayGroup, { density = 3.0, friction = 1.0, bounce = 0.1 } )


	----------------------------------------------------
	
	intOutsideTexturePosition_x = nil
	intOutsideTexturePosition_y = nil
	intInsideTexturePosition_x  = nil
	intInsideTexturePosition_y  = nil
end

function block:enablePhysics()
   physics.addBody( self.imageOutside, "static",{ density = 100.0, friction = 1.0, bounce = 0.0 } )
   physics.addBody( self.imageInside, "static",{ density = 100.0, friction = 1.0, bounce = 0.0 } )
   physics.newJoint( "weld", self.imageOutside, self.imageInside, 0, 0 )
end

function block:getImageInside()
	return self.imageInside
end
function block:getImageOutside()
	return self.imageOutside
end

function block:removeFromDisplayGroup( objDisplayGroup )
	objDisplayGroup:remove( self.imageOutside )
	objDisplayGroup:remove( self.imageInside  )	
end

function block:insertInDisplayGroup( objDisplayGroup )
	objDisplayGroup:insert( self.imageOutside )
	objDisplayGroup:insert( self.imageInside  ) 
end

function block:position( iX, iY )
	self.imageOutside.x = iX
	self.imageOutside.y = iY
	self.imageInside.x  = iX
	self.imageInside.y  = iY
end

function block:move( iX, iY )
	self.imageOutside.x = self.imageOutside.x + iX
	self.imageOutside.y = self.imageOutside.y + iY
	self.imageInside.x  = self.imageInside.x  + iX
	self.imageInside.y  = self.imageInside.y  + iY
end

function block:setBodyType( strBodyType )
	self.imageOutside.bodyType = strBodyType 
	self.imageInside.bodyType  = strBodyType 
end

function block:uninit()
	self.imageInside:removeSelf()
	self.imageOutside:removeSelf()
	self:clear()
	
end


