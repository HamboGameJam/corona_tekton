require ("middleclass")
require ("stringutils")

block_type = class( 'block_type' )

function block_type:initialize( arrInsideColors, arrOutsideColors )
	self.insidecolors = arrInsideColors
	self.outsidecolors = arrOutsideColors

end

function block_type:GetInsideColor()

	return self.insidecolors[ math.random( 1, #self.insidecolors ) ] 
	
end

function block_type:GetOutsideColor()

	return self.outsidecolors[ math.random( 1, #self.outsidecolors ) ] 
	
end
