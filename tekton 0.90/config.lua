application =
{
    content =
    {
        width = 320,
        height = 480,
        scale = "zoomEven",
        xAlign = "center",
        yAlign = "center",
        --[[
        imageSuffix =
        {
            ["@2x"] = 2
        },
        --]]
    },
}