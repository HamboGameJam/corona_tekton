function GetRandomColor()
	arrColors = { "black","blue","cyan","gold","gray","green","orange", "purple", "red","white" }
	return arrColors[ math.random(1, 10) ]
end

function GetColorRGBValue( strColorName )
	local iRValue
	local iGValue
	local iBValue
	
	if strColorName == "black" then
		iRValue = 0
		iGValue = 0
		iBValue = 0
	elseif strColorName == "blue" then
		iRValue = 10
		iGValue = 15
		iBValue	= 250
	elseif strColorName == "cyan" then
		iRValue = 10
		iGValue = 255
		iBValue	= 255
	elseif strColorName == "gold" then
		iRValue = 255
		iGValue = 255
		iBValue	= 10
	elseif strColorName == "gray" then
		iRValue = 120
		iGValue = 120
		iBValue = 120
	elseif strColorName == "green" then
		iRValue = 0
		iGValue = 255
		iBValue	= 0
	elseif strColorName == "orange" then
		iRValue = 255
		iGValue = 175
		iBValue	= 0
	elseif strColorName == "purple" then
		iRValue = 240
		iGValue = 0
		iBValue	= 240
	elseif strColorName == "red" then
		iRValue = 255
		iGValue = 0
		iBValue = 0	
	elseif strColorName == "white" then
		iRValue = 245 
		iGValue = 245
		iBValue = 245
	end
	
	
	return iRValue, iGValue, iBValue
end