require ( "gui_button"   )
require ( "gui_pattern" )

gui_button_kuklos = class('gui_button_kuklos', gui_button )

-- pass global data
function gui_button_kuklos:initialize( strObjName, objSharedData )
	gui_button:initialize()
	
	self.name                         = strObjName	
	self.imageKuklos                  = self:initialize_imageKuklos()
	
	local fRadius                     = self.imageKuklos.width * .33
	
	self.displayGroupCircleGradient   = self:initialize_displayGroupCircleGradient( fRadius )	
	self.displayGroupCircleGradient.x = self.imageKuklos.x - fRadius
	self.displayGroupCircleGradient.y = self.imageKuklos.y - fRadius
	
	self.guiPattern                   = self:initialize_guiPattern( objSharedData )
	
	self.imageHilight                 = self:initialize_imageHilight()
	self.imageHilight.width           = self.displayGroupCircleGradient.width	 
	self.imageHilight.height          = self.displayGroupCircleGradient.height
	self.imageHilight.x               = self.displayGroupCircleGradient.x + (self.displayGroupCircleGradient.width  * 0.5)
	self.imageHilight.y               = self.displayGroupCircleGradient.y + (self.displayGroupCircleGradient.height * 0.5)
	
	
	
	--local imageRingPattern = display.newImage( "images/pattern_circle.png" )
	--imageKuklos.x = display.contentWidth - (imageKuklos.width * .75)
	--imageKuklos.y = imageKuklos.height + 5
	
	--imageRingPattern.width  = imageKuklos.width	
	--imageRingPattern.height = imageKuklos.height
	--imageRingPattern.x      = imageKuklos.x
	--imageRingPattern.y      = imageKuklos.y
	--------------------------------------------------------------------
		
	--arrRGBAColorOutside = { r=080, g=000, b=147, a=20 }
	--arrRGBAColorInside  = { r=170, g=000, b=201, a=100 }
	
	--local radius = (imageKuklos.width * .35)
	
	--local objDisplayGroupCircleGradient = DisplayGroupGetRadialGradient( arrRGBAColorOutside, arrRGBAColorInside, radius )
	--objDisplayGroupCircleGradient.x = imageKuklos.x - (radius)
	--objDisplayGroupCircleGradient.y = imageKuklos.y - (radius)
	
	--local objDisplayObjectSphereHilight  = display.newImage( "images/sphere_hilight.png" )


	


	
	self.objDisplayGroup.x = display.contentWidth - (self.objDisplayGroup.width * 1.3)
	self.objDisplayGroup.y = (self.objDisplayGroup.height * .5)
--[[	
	local iPatternBlockIndex
	local iPatternBlockQty = 7
	
	self.PatternBlocks = {}
	
	for iPatternBlockIndex = 1, iPatternBlockQty, 1 do
	
		local iXPosition = 0 
		local iYPosition = 0
		local arrStrColorsInside = {"red", "gold"} 
		local arrStrColorsOutside = {"blue","cyan","green"}
		

		iXPosition, iYPosition = gui_button_kuklos:getPatternBlockPosition( 7, iPatternBlockIndex )

		print( "iXPosition, iYPosition" )
		print(  iXPosition, iYPosition )
		 		
		local _patternBlock = patternblock:new( iXPosition, iYPosition, arrStrColorsInside, arrStrColorsOutside )
		_patternBlock:insertInDisplayGroup( self.objDisplayGroup )
		
		self.PatternBlocks[ #self.PatternBlocks+1 ] = _patternBlock
		
	end
--]]	 
	--
		
end

function gui_button_kuklos:initialize_guiPattern( objSharedData )
	local guiPattern = gui_pattern:new( "GUI Pattern", objSharedData )
	guiPattern:insertInDisplayGroup( self.objDisplayGroup )
	
	return guiPattern
end

function gui_button_kuklos:initialize_imageHilight()
    local imageSphereHilight  = display.newImage( "images/sphere_hilight.png" )
    imageSphereHilight.alpha  = 0.80
	--local imageKuklos = display.newImage( "images/kuklos.png" )
	
	self.objDisplayGroup:insert( imageSphereHilight )
	
	return imageSphereHilight
end

function gui_button_kuklos:initialize_imageKuklos()
	local imageKuklos = display.newImage( "images/kuklos.png" )
	
	self.objDisplayGroup:insert( imageKuklos )
	
	return imageKuklos
	
end

function gui_button_kuklos:initialize_displayGroupCircleGradient( fRadius )
	arrRGBAColorOutside = { r=080, g=000, b=147, a=020 }
	arrRGBAColorInside  = { r=170, g=000, b=201, a=100 }	
	
	local objDisplayGroupCircleGradient = DisplayGroupGetRadialGradient( arrRGBAColorOutside, arrRGBAColorInside, fRadius )
	
	self.objDisplayGroup:insert( objDisplayGroupCircleGradient )
	
	return objDisplayGroupCircleGradient
	
end

function gui_button_kuklos:update()
	self.guiPattern:update()
	--[[
	if( self ~= nil ) then
		if( self.PatternBlocks ~= nil ) then
			if( #self.PatternBlocks > 1 ) then
			
				print( "\n #self.PatternBlocks " )
				print( #self.PatternBlocks )
				
				for iPatternBlockIndex = 1, #self.PatternBlocks, 1 do
					print( "\n gui_button_kuklos:update() "..iPatternBlockIndex )
					self.PatternBlocks[ iPatternBlockIndex ]:update()
				end
			end				
		end
	end
	--]]
end