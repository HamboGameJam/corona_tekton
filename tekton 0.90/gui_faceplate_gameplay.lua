require ( "gui_faceplate" )
require ( 'utils_displaygroup' )
require ( 'DataDumper' )


function dump(...)
  print(DataDumper(...), "\n---")
end

gui_faceplate_gameplay = class( 'gui_faceplate_gameplay', gui_faceplate )

function gui_faceplate_gameplay:initialize( strObjectName )
	gui_faceplate.initialize( self, strObjectName )
	
	local optionsImageSheetBlackMarble =
	{
		frames = {
			-- FRAME 1:
			{
	            x = 0,
	            y = 0,
	            width  = (display.contentWidth       ),
	            height = 50
	        },
	        -- FRAME 2:
	        {
	            x = 0,
	            y = (display.contentHeight * .20),
	            width  = (display.contentWidth       ),
	            height = 55
	        },
		},
		
		sheetContentWidth  = 512,
		sheetContentHeight = 512
	}
	
	local imageSheetBlackMarble   = graphics.newImageSheet( "images/color_black.jpg", optionsImageSheetBlackMarble )
	local imageBlackMarbleFloor   = display.newImage( imageSheetBlackMarble, 1 )
	local imageBlackMarbleCeiling = display.newImage( imageSheetBlackMarble, 2 )
	
	imageBlackMarbleCeiling.x = display.contentWidth * .5
	imageBlackMarbleCeiling.y = imageBlackMarbleCeiling.height * .5
	imageBlackMarbleFloor.x   = display.contentWidth * .5
	imageBlackMarbleFloor.y   = display.contentHeight - (imageBlackMarbleFloor.height * .5)
	
	local imageMosaicStripIndigoTop = display.newImageRect( "images/mosaic_strip_indigo.png", display.contentWidth, 40, true )
	imageMosaicStripIndigoTop.x = imageMosaicStripIndigoTop.width * .5
	imageMosaicStripIndigoTop.y = imageMosaicStripIndigoTop.height * .5 + 10
	imageMosaicStripIndigoTop:scale( 1, -1 )
		
	local imageMosaicStripIndigoBottom = display.newImageRect( "images/mosaic_strip_indigo.png", display.contentWidth, 40, true )
	imageMosaicStripIndigoBottom.x = imageMosaicStripIndigoBottom.width * .5
	imageMosaicStripIndigoBottom.y = display.contentHeight - (imageMosaicStripIndigoBottom.height * .5 )- 8

	
	
	local intColumnHeight = display.contentHeight - (imageBlackMarbleCeiling.height + imageBlackMarbleFloor.height)
	local imageColumnLeft = display.newImageRect( "images/column_left.png", 36, intColumnHeight, true )
	local imageColumnRight = display.newImageRect( "images/column_right.png", 36, intColumnHeight, true )
	
	print( "\nintColumnHeight - ".. intColumnHeight .. "\nWidth".. imageColumnLeft.width )
	
	imageColumnLeft.x  = imageColumnLeft.width + 9
	imageColumnLeft.y  = imageColumnLeft.height *.5 + imageBlackMarbleCeiling.height
	imageColumnRight.x = display.contentWidth - imageColumnRight.width - 9
	imageColumnRight.y = imageColumnRight.height *.5 + imageBlackMarbleCeiling.height
	
	self.objDisplayGroup:insert(imageColumnLeft)
	self.objDisplayGroup:insert(imageColumnRight)
	self.objDisplayGroup:insert(imageBlackMarbleFloor)
	self.objDisplayGroup:insert(imageBlackMarbleCeiling)
	self.objDisplayGroup:insert(imageMosaicStripIndigoTop)
	self.objDisplayGroup:insert(imageMosaicStripIndigoBottom)
end

function gui_faceplate_gameplay:update()
	--gui_faceplate.update(self)

end