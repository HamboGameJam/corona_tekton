require ("middleclass")
require ("stringutils")
require ( "patternblock" )

gui_pattern = class( 'gui_pattern' )

function gui_pattern:initialize( strObjName, objSharedData )
	self.name              = strObjName
	self.objSharedData     = objSharedData
	self.objDisplayGroup   = display.newGroup()
	
	local iPatternBlockIndex	
	local iPatternBlockQty = 7
	
	self.PatternBlocks = {}
	
	for iPatternBlockIndex = 1, iPatternBlockQty, 1 do
	
		local iXPosition = 0 
		local iYPosition = 0
		local arrStrColorsInside = {"red", "gold"} 
		local arrStrColorsOutside = {"blue","cyan","green"}
		

		iXPosition, iYPosition = gui_pattern:getPatternBlockPosition( 7, iPatternBlockIndex )

		print( "iXPosition, iYPosition" )
		print(  iXPosition, iYPosition )
		 		
		local _patternBlock = patternblock:new( iXPosition, iYPosition, arrStrColorsInside, arrStrColorsOutside )
		_patternBlock:insertInDisplayGroup( self.objDisplayGroup )
		
		self.PatternBlocks[ #self.PatternBlocks+1 ] = _patternBlock
		
	end	
end

function gui_pattern:getPatternBlockPosition( iNumberOfBlocks, iBlockIndex )
--[[
         1   2
       3   4   5
         6   7
--]]
	
	local arrBlockPositions = { {30,20},{52,20},{17,40},{40,40},{64,40},{30,60},{52,60} }
	local arrBlockPatterns  = { {4}, {3,5}, {3,4,5}, {1,2,6,7}, {1,2,4,6,7}, {1,2,3,5,6,7}, {1,2,3,4,5,6,7} }
   
   	local iX = ( arrBlockPositions[ ( arrBlockPatterns[iNumberOfBlocks] )[iBlockIndex] ] )[1]
   	local iY = ( arrBlockPositions[ ( arrBlockPatterns[iNumberOfBlocks] )[iBlockIndex] ] )[2]
   	
   	print( "\niX" )
   	print( iX )
   	
   	print( "\niY" )
   	print( iY )   	
   	
	return iX, iY
end

function gui_pattern:insertInDisplayGroup( objDisplayGroup )
	objDisplayGroup:insert( self.objDisplayGroup  )	
end

function gui_pattern:removeFromDisplayGroup( objDisplayGroup )
	objDisplayGroup:remove( self.objDisplayGroup  )
end

function gui_pattern:update()
	if( self ~= nil ) then
		if( self.PatternBlocks ~= nil ) then
			if( #self.PatternBlocks > 1 ) then
			
				print( "\n #self.PatternBlocks " )
				print( #self.PatternBlocks )
				
				for iPatternBlockIndex = 1, #self.PatternBlocks, 1 do
					print( "\n gui_button_kuklos:update() "..iPatternBlockIndex )
					self.PatternBlocks[ iPatternBlockIndex ]:update()
				end
			end				
		end
	end
	
end