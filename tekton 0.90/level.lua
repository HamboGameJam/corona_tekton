require ("middleclass")
require ("piece_type")
require ("block_type")
require ("pattern_type")
--require( "xml" )

level = class('level')

function level:initialize( strLevelFileName )
	self.file_name    = strLevelFileName
	self.title        = nil
	self.pieceTypes   = nil
	self.blockTypes   = nil
	self.patternTypes = nil
	self.goal         = nil

end

function level:getRandomBlockType()
	return self.blockTypes[  math.random( 1, #self.blockTypes )  ]
end

function level:getRandomPieceType()
	return self.pieceTypes[  math.random( 1, #self.pieceTypes )  ]
end

function level:getBlockTypes()
	return self.blockTypes
end

function level:getPieceTypes()
	return self.pieceTypes
end

function level:getPatternTypes()
	return self.patternTypes
end

function level:getTitle()
	return self.title
end

function level:load()
	local xml = require( "xml" ).newParser()

	print( "\n self.file_name load" )
	print( self.file_name )

	local loadedXmlInfo = xml:loadFile( self.file_name )

	local iLevelChildIndex = 0
	
	for iLevelChildIndex=1,#loadedXmlInfo.child do			
		
		if loadedXmlInfo.child[iLevelChildIndex].name == "title" then
		
			self.title = loadedXmlInfo.child[iLevelChildIndex].value
			
		elseif loadedXmlInfo.child[iLevelChildIndex].name == "dropdelay" then
		
			self.dropdelay = loadedXmlInfo.child[iLevelChildIndex].value
			
		elseif loadedXmlInfo.child[iLevelChildIndex].name == "pieces" then
		
			self.pieceTypes = {}
			
			for iPiecesChildIndex=1, #loadedXmlInfo.child[iLevelChildIndex].child do
				local pieceType = piece_type:new( 0, 0 )
				
				pieceType.blockquantity = loadedXmlInfo.child[iLevelChildIndex].child[iPiecesChildIndex].child[1].value  
				pieceType.blocksize     = loadedXmlInfo.child[iLevelChildIndex].child[iPiecesChildIndex].child[2].value
				
				self.pieceTypes[ #self.pieceTypes + 1 ] = pieceType
						
			end
			
		elseif loadedXmlInfo.child[iLevelChildIndex].name == "blocks" then
		
			self.blockTypes = {}
			
			for iBlocksChildIndex=1, #loadedXmlInfo.child[iLevelChildIndex].child do

				local insideColors = {}
				for iColorInsideIndex=1, #loadedXmlInfo.child[iLevelChildIndex].child[iBlocksChildIndex].child[1].child do
					insideColors[  #insideColors + 1  ] =
						loadedXmlInfo.child[iLevelChildIndex].
							child[iBlocksChildIndex].
							child[1].
							child[iColorInsideIndex].value
				end
				

				local outsideColors = {}
				for iColorOutsideIndex=1, #loadedXmlInfo.child[iLevelChildIndex].child[iBlocksChildIndex].child[2].child do
					outsideColors[  #outsideColors + 1  ] =
						loadedXmlInfo.child[iLevelChildIndex].
							child[iBlocksChildIndex].
							child[2].
							child[iColorOutsideIndex].value
				end

				local blockType = block_type:new( insideColors, outsideColors )
				self.blockTypes[  #self.blockTypes + 1  ] = blockType

			end
		elseif loadedXmlInfo.child[iLevelChildIndex].name == "patterns" then
		
			self.patternTypes = {}
			for iPatternChildIndex=1, #loadedXmlInfo.child[iLevelChildIndex].child do
				local strRangeBlockQuantity = ""
				local insideColors          = {}
				local outsideColors         = {}
				
				local strTagName            = ""
				
				-- 3 values: BlockQty, OutsideColors, InsideColors 
				for iPatternValueChildIndex=1, 3 do
					strTagName = loadedXmlInfo.child[iLevelChildIndex].child[iPatternChildIndex].child[iPatternValueChildIndex].name
					 
					if strTagName == "blockquantity" then					
						strRangeBlockQuantity = loadedXmlInfo.child[iLevelChildIndex].
													child[iPatternChildIndex].
													child[iPatternValueChildIndex].value
						
					elseif strTagName == "insidecolor" then
						local iInsideColorIndex
						
						for iInsideColorIndex = 1, #loadedXmlInfo.child[iLevelChildIndex].child[iPatternChildIndex].child[iPatternValueChildIndex].child do
						
							insideColors[#insideColors+1] = loadedXmlInfo.child[iLevelChildIndex].
								child[iPatternChildIndex].
								child[iPatternValueChildIndex].
								child[iInsideColorIndex].value
														
						end
						
					elseif strTagName == "outsidecolor" then
						local iOutsideColorIndex
						
						for iOutsideColorIndex = 1, #loadedXmlInfo.child[iLevelChildIndex].
													 child[iPatternChildIndex].
													 child[iPatternValueChildIndex].child do
						
							outsideColors[#outsideColors+1] = loadedXmlInfo.child[iLevelChildIndex].
								child[iPatternChildIndex].
								child[iPatternValueChildIndex].
								child[iOutsideColorIndex].value
														
						end
						
					end

				end
				
				local patternType = pattern_type:new( strRangeBlockQuantity, insideColors, outsideColors )
				self.patternTypes[  #self.patternTypes + 1 ] = patternType

			end
		elseif loadedXmlInfo.child[iLevelChildIndex].name == "goal" then
			self.goal = loadedXmlInfo.child[iLevelChildIndex].value
		end
	end
end