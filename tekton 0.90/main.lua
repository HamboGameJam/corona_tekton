require ("shared_data")
--require ("parallanx_nightsky")
--require ("physics_layer_gameplay")
--require ("gui_faceplate_gameplay")
--require ("gui_button_kuklos")
require ("statemachine")


sharedData = shared_data:new()

sharedData.stateMachine:changeState( "intro" )




--local _parallanx_nightsky     = parallanx_nightsky:new( "Parallanx NightSky", 1, 0 )
--local _physics_layer_gameplay = physics_layer_gameplay:new( "Physics Layer Gameplay", sharedData )


--local _gui_faceplate_gameplay = gui_faceplate_gameplay:new( "GUI Faceplate Gameplay" )
--local _gui_button_kuklos      = gui_button_kuklos:new( "GUI Button Kuklos", sharedData  )

local function update( event )
	--_parallanx_nightsky:update()
	--_gui_button_kuklos:update()
	
	sharedData.stateMachine:update()
	
end

timer.performWithDelay(1, update, -1)