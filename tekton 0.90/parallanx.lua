--module(..., package.seeall)
require ("middleclass")

function dump(...)
  print(DataDumper(...), "\n")
end


plate = class('plate')
	function plate:initialize( strName, displayObject, functionDeltaX, functionDeltaY )
		self.name             = strName 
		self.displayObject    = displayObject
		--[[
		print( "\n-plate:initialize1----" )
		print( "\nself.Name            "..DataDumper(self.name) )
		print( "\nself.displayObject.x "..DataDumper(self.displayObject.x) )
		print( "\nself.displayObject.y "..DataDumper(self.displayObject.y) )
		print( "\nself.displayObject.xReference "..DataDumper(self.displayObject.xReference) )
		print( "\nself.displayObject.yReference "..DataDumper(self.displayObject.yReference) )
		print( "\nself.displayObject.width  "..DataDumper(self.displayObject.width) )
		print( "\nself.displayObject.height "..DataDumper(self.displayObject.height) )
		print( "\n-plate:initialize1----" )
		--]]		
		--self.displayObject.x           = display.contentWidth      * 0.5
		--self.displayObject.y           =(display.contentHeight     * 0.5)	
		--self.displayObject.xReference  = self.displayObject.width  * 0.5
		--self.displayObject.yReference  = self.displayObject.height * 0.5
		
		self.functionDeltaX = functionDeltaX
		self.functionDeltaY = functionDeltaY
		self.functionUpdate = functionUpdate
		--[[
		print( "\n-plate:initialize2----" )
		print( "\nself.Name            "..DataDumper(self.name) )
		print( "\nself.displayObject.x "..DataDumper(self.displayObject.x) )
		print( "\nself.displayObject.y "..DataDumper(self.displayObject.y) )
		print( "\nself.displayObject.xReference "..DataDumper(self.displayObject.xReference) )
		print( "\nself.displayObject.yReference "..DataDumper(self.displayObject.yReference) )
		print( "\nself.displayObject.width  "..DataDumper(self.displayObject.width) )
		print( "\nself.displayObject.height "..DataDumper(self.displayObject.height) )
		
		print( "\n-plate:initialize2----" )
		--]]
	end
	
	function plate:update( intDeltaX, intDeltaY )
		--self.functionUpdate( intDeltaX, intDeltaY )
	end
	


 

	plateTiledGroup = class('plateTiledGroup', plate )

	function plateTiledGroup:initialize( strName, displayObject, functionDeltaX, functionDeltaY )
		plate.initialize( self, strName, displayObject, functionDeltaX, functionDeltaY )
	end
	
	function plateTiledGroup:update( intDeltaX, intDeltaY )	
		--[[
		print( "\nplateTiledGroup:update( "..intDeltaX..","..intDeltaY..")" )
		print( "\n-plate:initialize2----" )
		print( "\nself.Name            "..DataDumper(self.name) )
		print( "\nself.displayObject.x "..DataDumper(self.displayObject.x) )
		print( "\nself.displayObject.y "..DataDumper(self.displayObject.y) )
		print( "\nself.displayObject.xReference "..DataDumper(self.displayObject.xReference) )
		print( "\nself.displayObject.yReference "..DataDumper(self.displayObject.yReference) )
		print( "\nself.displayObject.width  "..DataDumper(self.displayObject.width) )
		print( "\nself.displayObject.height "..DataDumper(self.displayObject.height) )
		
		print( "\nplateTiledGroup:update( "..intDeltaX..","..intDeltaY..")" )
		--]]
		--[[
		local function GetRowOrColumnTiles( charRowOrColumn_blrt )
			local arrRowOrColumn = {}
			
			for intTileIndex = 1, self.displayObject.numChildren, 1 do
				if string.find( self.displayObject[intTileIndex].name, charRowOrColumn_blrt ) ~= nil then
					arrRowOrColumn[#arrRowOrColumn+1] = self.displayObject[intTileIndex]
				end	
			end
			
			return arrRowOrColumn
		end
		--]]
		
		intPlateDeltaX = self.functionDeltaX( intDeltaX )
		intPlateDeltaY = self.functionDeltaY( intDeltaY )
		--print( "\nintPlateDeltaX "..intPlateDeltaX )
		--print( "\nintPlateDeltaY "..intPlateDeltaY )		
		---[[
		self.displayObject.x = self.displayObject.x + intPlateDeltaX
		self.displayObject.y = self.displayObject.y + intPlateDeltaY
		
		if intPlateDeltaX ~= 0 then
			if intPlateDeltaX < 0 then
				if (self.displayObject.x -  (self.displayObject.width * 0.66)) <= 0 then
					self.displayObject.x = 
						self.displayObject.x + (self.displayObject.width * 0.33 )
					--[[
					local arrLeftColumn = GetRowOrColumnTiles("l")
					local arrRightColumn = GetRowOrColumnTiles("r")
					arrLeftColumn[1].x = arrLeftColumn[1].x + self.displayObject.width
					arrLeftColumn[2].x = arrLeftColumn[2].x + self.displayObject.width
					arrLeftColumn[1].name = string.gsub(arrLeftColumn[1].name, "l", "r")
					arrLeftColumn[2].name = string.gsub(arrLeftColumn[2].name, "l", "r")
					arrRightColumn[1].name = string.gsub(arrRightColumn[1].name, "r", "l")
					arrRightColumn[2].name = string.gsub(arrRightColumn[2].name, "r", "l")
					--]]
				end
			else -- intPlateDeltaX > 0 
				if (self.displayObject.x ) >= display.contentWidth then
					self.displayObject.x = 
						self.displayObject.x - (self.displayObject.width * 0.33 )
					--[[
					local arrLeftColumn = GetRowOrColumnTiles("l")
					local arrRightColumn = GetRowOrColumnTiles("r")
					arrRightColumn[1].x = arrRightColumn[1].x - self.displayObject.width
					arrRightColumn[2].x = arrRightColumn[2].x - self.displayObject.width
					arrLeftColumn[1].name = string.gsub(arrLeftColumn[1].name, "l", "r")
					arrLeftColumn[2].name = string.gsub(arrLeftColumn[2].name, "l", "r")
					arrRightColumn[1].name = string.gsub(arrRightColumn[1].name, "r", "l")
					arrRightColumn[2].name = string.gsub(arrRightColumn[2].name, "r", "l")
					--]]
				end		
			end
		end
		
		if intPlateDeltaY ~= 0 then
			
			--if intPlateDeltaY < 0 then
	
			--else -- intPlateDeltaY > 0
			
			--end
			
		end
		--]]
	end


parallanx = class('parallanx')
function parallanx:initialize( strName, intDeltaX, intDeltaY )
	self.name = strName
	self.tablePlates = {}
	self.intDeltaX = intDeltaX
	self.intDeltaY = intDeltaY
	--[[	
	print( "\n-parallanx:initialize----" )	
	print( "\nself.name       "..self.name )
	print( "\nself.intDeltaX  "..self.intDeltaX )
	print( "\nself.intDeltaY  "..self.intDeltaY )	
	print( "\n-parallanx:initialize----" )
	--]]	
end

function parallanx:insertPlate( plateObj )
	--[[
	print( "\n-parallanx:insertPlate" )
	print( "\n#self.tablePlates  "..#self.tablePlates )
	print( "\nplateObj.displayObject.x  "..plateObj.displayObject.x )
	--]]
	table.insert( self.tablePlates,  plateObj )
	--[[
	print( "\n#self.tablePlates  "..#self.tablePlates )
	print( "\n-parallanx:insertPlate" )
	--]]
end


function parallanx:update()
	--self.tablePlates[2].text = "("..self.intDeltaX..", "..self.intDeltaY..")"

	for intPlateindex, aPlate in ipairs( self.tablePlates ) do	
	--[[	
		print( "\n-parallanx:update" )
		print( "\nself.tablePlates[intPlateindex]  "..self.tablePlates[intPlateindex].name   )
		print( "\nintPlateindex        "..DataDumper(intPlateindex)  )
		print( "\nself.intDeltaX       "..DataDumper(self.intDeltaX) )
		print( "\nself.intDeltaY       "..DataDumper(self.intDeltaY) )
		print( "\naPlate               "..DataDumper(aPlate.name)    )
		print( "\n-parallanx:update" )
	--]]			
		aPlate:update( self.intDeltaX, self.intDeltaY )

	end
end