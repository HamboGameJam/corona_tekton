--module(..., package.seeall)
require ( "parallanx" )
require ( 'utils_displaygroup' )
require ( 'DataDumper' )

function dump(...)
  print(DataDumper(...), "\n---")
end


parallanx_nightsky = class( 'parallanx_nightsky', parallanx )

function parallanx_nightsky:initialize( strObjectName, intDeltaX, intDeltaY )
	parallanx.initialize( self, strObjectName, intDeltaX, intDeltaY )
  
	local gradientPurple   = graphics.newGradient(
							 	{ 005, 000, 032 },	
							 	{ 129, 000, 108 },
							 	{ 005, 000, 032 },
							 	{ 021, 000, 072 },						 	
							 	"down" )

							 	
	local objDisplayRectGradientPurple = display.newRect( (display.contentWidth  * -.10),
														  (display.contentHeight * -.10),
														  display.contentWidth  + (display.contentWidth  * .20),
														  display.contentHeight + (display.contentHeight * .20)  )
	objDisplayRectGradientPurple:setFillColor( gradientPurple )
	
	
	local objDisplayGroupStars =
  		DisplayGroupGetTilesFromImage( "DisplayGroupStars",
  									   "images/tile_stars_0.png",
  									   512, 512 )
	DisplayGroupSetFillColor( objDisplayGroupStars, 252, 206, 165 )
	objDisplayGroupStars.alpha = 1

	local objDisplayGroupCloud0 =
		DisplayGroupGetTilesFromImage( "DisplayGroupCloud0",
  									   "images/tile_cloud_0.png",
  									   512, 512 ) 
  	DisplayGroupSetFillColor( objDisplayGroupCloud0, 5, 5, 40 )
 	objDisplayGroupCloud0.alpha = 0.85
  
	local objDisplayGroupCloud1 =
		DisplayGroupGetTilesFromImage( "DisplayGroupCloud1",
  									   "images/tile_cloud_1.png",
  									   512, 512 ) 
  	DisplayGroupSetFillColor( objDisplayGroupCloud1, 75, 055, 125 )
 	objDisplayGroupCloud1.alpha = 0.50 	
  
	local objDisplayGroupCloud2 =
		DisplayGroupGetTilesFromImage( "DisplayGroupCloud2",
									   "images/tile_cloud_2.png",
  									   512, 512 )
	DisplayGroupSetFillColor( objDisplayGroupCloud2, 200, 150, 170 )
	objDisplayGroupCloud2.alpha = 0.25
  
	local functionDeltaStopped           = function(intDelta) return (0               ) end
	local functionDeltaMultiplierFull    = function(intDelta) return (intDelta * 0.75 ) end
	local functionDeltaMultiplierHalf    = function(intDelta) return (intDelta * 0.5  ) end
	local functionDeltaMultiplierQuarter = function(intDelta) return (intDelta * 0.25 ) end
	local functionDeltaMultiplierEighth  = function(intDelta) return (intDelta * 0.125) end
  
 ---[[ 
  local plateGradientPurple = 
  	plate:new( "plateGradientPurple",
			   objDisplayRectGradientPurple, 
			   functionDeltaStopped,
			   functionDeltaStopped )
--]]			   

    			   
  local plateDisplayGroupStars = 
    plateTiledGroup:new( "plateDisplayGroupStars",
						 objDisplayGroupStars, 
						 functionDeltaMultiplierEighth,
						 functionDeltaStopped )
			   
  local plateDisplayGroupCloud0 = 
    plateTiledGroup:new( "plateDisplayGroupCloud0",
						 objDisplayGroupCloud0, 
						 functionDeltaMultiplierQuarter,
						 functionDeltaStopped )
			   
  local plateDisplayGroupCloud1 = 
    plateTiledGroup:new( "plateDisplayGroupCloud1",
						 objDisplayGroupCloud1, 
						 functionDeltaMultiplierHalf,
						 functionDeltaStopped )
			    			   
  local plateDisplayGroupCloud2 = 
    plateTiledGroup:new( "plateDisplayGroupCloud2",
						 objDisplayGroupCloud2, 
						 functionDeltaMultiplierFull,
						 functionDeltaStopped )			   
			   
  parallanx.insertPlate( self, plateGradientPurple )
  parallanx.insertPlate( self, plateDisplayGroupStars )
  parallanx.insertPlate( self, plateDisplayGroupCloud0 )
  parallanx.insertPlate( self, plateDisplayGroupCloud1 )
  parallanx.insertPlate( self, plateDisplayGroupCloud2 )
   
end

function parallanx_nightsky:update()
	parallanx.update(self)
	
end
