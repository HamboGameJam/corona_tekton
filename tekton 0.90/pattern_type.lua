require ("middleclass")
require ("stringutils")

pattern_type = class( 'pattern_type' )

function pattern_type:initialize( strRangeBlockQuantity, arrInsideColors, arrOutsideColors )

	print( "\npattern_type:initialize" )
	
	print( "\npattern_type.strRangeBlockQuantity" )
	print( strRangeBlockQuantity )
	print( "\npattern_type.arrInsideColors" )
	print( #arrInsideColors )
	print( "\npattern_type.arrOutsideColors" )
	print( #arrOutsideColors )
	
	self.blockquantity = strRangeBlockQuantity
	self.insidecolors = arrInsideColors
	self.outsidecolors = arrOutsideColors

end

function pattern_type:GetBlockQuantity()
	iNumber1, iNumber2 = GetNumbersInRange( self.blockquantity )
	return math.random( iNumber1, iNumber2 )
	
end

function pattern_type:GetInsideColor()

	return self.insidecolors[ math.random( 1, #self.insidecolors ) ] 
	
end

function pattern_type:GetOutsideColor()

	return self.outsidecolors[ math.random( 1, #self.outsidecolors ) ] 
	
end

