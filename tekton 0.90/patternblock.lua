require ("middleclass")
require ('gameutils')

patternblock = class('patternblock')


function patternblock:initialize( iXPosition, iYPosition, arrColorsInside, arrColorsOutside )

	self.strColorInsideCurrent  = arrColorsInside[  math.random( #arrColorsInside  ) ]
	self.strColorOutsideCurrent = arrColorsOutside[ math.random( #arrColorsOutside ) ]
	self.strColorInsideNext     = arrColorsInside[  math.random( #arrColorsInside  ) ]
	self.strColorOutsideNext    = arrColorsOutside[ math.random( #arrColorsOutside ) ]
	
	print( "\nself.strColorInsideCurrent:".. self.strColorInsideCurrent   )
	print( "\nself.strColorOutsideCurrent:".. self.strColorOutsideCurrent )
	print( "\nself.strColorInsideNext:".. self.strColorInsideNext         )
	print( "\nself.strColorOutsideNext:".. self.strColorOutsideNext       )
	
	self.arrColorsInside  = arrColorsInside
	self.arrColorsOutside = arrColorsOutside
	
	local fTransitionPercentage = 0.0
	self.fTransitionPercentage = fTransitionPercentage
	--left top width height
	

	
	self.objDisplayRect = display.newRect( iXPosition, iYPosition, 12, 12 ) 

	--self.objDisplayRect.x = iXPosition
	--self.objDisplayRect.y =	iYPosition
	
	self.objDisplayRect.strokeWidth = 3
	--self.objDisplayRect.isVisible   = false
	
	self:updateInsideColor()
	self:updateOutsideColor()	
			
end

function patternblock:isVisilble()
	return self.objDisplayRect.isVisible 
end

function patternblock:isVisilble( bIsVisible )
	self.objDisplayRect.isVisible = bIsVisible
end

function patternblock:insertInDisplayGroup( objDisplayGroup )
	objDisplayGroup:insert( self.objDisplayRect  ) 
end

function patternblock:removeFromDisplayGroup( objDisplayGroup )
	objDisplayGroup:remove( self.objDisplayRect  ) 
end

function patternblock:getTransitionPercentage()
	return self.fTransitionPercentage
end

function patternblock:setTransitionPercentage( _fTransitionPercentage )
	self.fTransitionPercentage = _fTransitionPercentage
end

function patternblock:getRandomInsideColor()
	return self.arrColorsInside[ math.random( #self.arrColorsInside ) ]
end

function patternblock:getRandomOutsideColor()
	return self.arrColorsOutside[ math.random( #self.arrColorsOutside ) ]
end

function patternblock:getInsideColors()
	return self.arrColorsInside
end

function patternblock:updateInsideColor()
	
	local iInsideCurrent_R
	local iInsideCurrent_G
	local iInsideCurrent_B
	
	local iInsideNext_R
	local iInsideNext_G
	local iInsideNext_B
	
	iInsideCurrent_R, iInsideCurrent_G, iInsideCurrent_B = GetColorRGBValue( self.strColorInsideCurrent )
	iInsideNext_R,    iInsideNext_G,    iInsideNext_B    = GetColorRGBValue( self.strColorInsideNext )
	
	local iR = (  ( iInsideCurrent_R * (1-self.fTransitionPercentage) ) + ( iInsideNext_R * self.fTransitionPercentage )  ) * 0.5
	local iG = (  ( iInsideCurrent_G * (1-self.fTransitionPercentage) ) + ( iInsideNext_G * self.fTransitionPercentage )  ) * 0.5
	local iB = (  ( iInsideCurrent_B * (1-self.fTransitionPercentage) ) + ( iInsideNext_B * self.fTransitionPercentage )  ) * 0.5
		
	self.objDisplayRect:setFillColor( iR, iG, iB )
	
end

function patternblock:updateOutsideColor()
	
	local iOutsideCurrent_R
	local iOutsideCurrent_G
	local iOutsideCurrent_B
	
	local iOutsideNext_R
	local iOutsideNext_G
	local iOutsideNext_B
	
	iOutsideCurrent_R, iOutsideCurrent_G, iOutsideCurrent_B = GetColorRGBValue( self.strColorOutsideCurrent )
	iOutsideNext_R,    iOutsideNext_G,    iOutsideNext_B    = GetColorRGBValue( self.strColorOutsideNext )
	
	local iR = (  ( iOutsideCurrent_R * (1-self.fTransitionPercentage) ) + ( iOutsideNext_R * self.fTransitionPercentage )  ) * 0.5
	local iG = (  ( iOutsideCurrent_G * (1-self.fTransitionPercentage) ) + ( iOutsideNext_G * self.fTransitionPercentage )  ) * 0.5
	local iB = (  ( iOutsideCurrent_B * (1-self.fTransitionPercentage) ) + ( iOutsideNext_B * self.fTransitionPercentage )  ) * 0.5

	--print( "iR, iG, iB " )
	--print( iR, iG, iB )
		
	self.objDisplayRect:setStrokeColor( iR, iG, iB )
end

function patternblock:setInsideColors( arrColorsInside )
	self.arrColorsInside = arrColorsInside
end

function patternblock:getOutsideColors()
	return self.arrColorsOutside
end

function patternblock:setOutsideColors( arrColorsOutside )
	self.arrColorsOutside = arrColorsOutside
end

function patternblock:update()
	self.fTransitionPercentage = self.fTransitionPercentage + 0.025
	
	print( "self:getTransitionPercentage()" )
	print(  self:getTransitionPercentage()  )
	
	if ( self.fTransitionPercentage > 1.00 ) then	
		self.fTransitionPercentage = self.fTransitionPercentage - 1.00
		
		self.strColorInsideCurrent  = self.strColorInsideNext
		self.strColorOutsideCurrent = self.strColorOutsideNext
		self.strColorInsideNext     = self.arrColorsInside[  math.random( #self.arrColorsInside  ) ]
		self.strColorOutsideNext    = self.arrColorsOutside[ math.random( #self.arrColorsOutside ) ]
		

	end

	self:updateInsideColor()	
	self:updateOutsideColor()
		
end