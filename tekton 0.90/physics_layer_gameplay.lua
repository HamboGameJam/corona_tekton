require ("middleclass")
require ( 'gameutils' )
require ( 'piece_type' )
require ( 'block_type' )
require ( 'block' )
require ( 'piece' )
require ( 'level' )
require ( 'shared_data' )
require ( 'tprint' )

physics_layer_gameplay = class( 'physics_layer_gameplay' )

function physics_layer_gameplay:initialize( strObjName, sharedData )

	self.name         = strObjName
	self.level        = nil
	self.physics      = nil	
	self.shared_data  = nil
	self.shared_data  = sharedData
	
	self.objDisplayGroup = display.newGroup()   
		
	physics_layer_gameplay:initialize_physics()
	physics_layer_gameplay:initialize_physics_walls()
------------------
function onPostCollision( event )
	--local t = event.object1
	
	if( event.object1.id == "block") then		
		local block = event.object1.aBlock

	end
	
end

function onCollision( event )
    local phase = event.phase
    
    local obj1 = event.object1
    local obj2 = event.object2
    
    if ( phase == "began" ) then
    	--print("\nCollision began")
    elseif  ( phase == "ended" ) then
    	--print("\nCollision ended")
    end


end

------------------
	Runtime:addEventListener( "collision", onCollision )
	Runtime:addEventListener( "postCollision", onPostCollision )
		

--[[
	local function createBlock()
	
		_block = new block(  physics_layer_gameplay:GetRandomBlockPositionX(),
							 60,
							 25,
							 GetRandomColor(),
							 GetRandomColor()  )
	end
	
	local timerBlock = timer.performWithDelay( 1000, createBlock, 0 )
--]]



	local objLevel = level:new("levels/level_1.xml")
	objLevel:load()
	self.level = objLevel
	
	--patternType = objLevel.getPatternTypes()[1]
	
	--print( "\npatternType.GetBlockQuantity()" )
	--print( patternType.GetBlockQuantity() )
	--print( "\npatternType.GetInsideColor()" )
	--print( patternType.GetInsideColor() )
	--print( "\npatternType.GetOutsideColor()" )
	--print( patternType.GetOutsideColor() )
	
	pieceType = self.level:getPieceTypes()[1]
	
	local function displayLevelTitle(event)
		--print("\ndisplayLevelTitle")
		local textTitle = display.newText(objLevel:getTitle(), 0, 0, native.systemFont, 22)
		textTitle:setTextColor(255, 255, 255)

		
		--textName.text = objLevel.title
		textTitle.x    = display.contentWidth * .5
		textTitle.y    = display.contentHeight * .30
		--[[
		_block = new block(  physics_layer_gameplay:GetRandomBlockPositionX(),
							 60,
							 25,
							 GetRandomColor(),
							 GetRandomColor()  )
							 --]]
		local function removeLevelTitle(event)
			textTitle:removeSelf()
			textTitle = nil			
		end
		
		local timerIntroEnd = timer.performWithDelay( 4000, removeLevelTitle, 1 )
	end
	
	local timerIntroStart = timer.performWithDelay( 200, displayLevelTitle, 1 )
	
	local function spawnPiece( event )
		_blockType = self.level:getRandomBlockType()
		_pieceType = self.level:getRandomPieceType()
		
		local iBlockQuantity = _pieceType:GetBlockQuantity()
		local iBlockLength   = _pieceType:GetBlockSize()
		
		local strBlockColorOutside = _blockType:GetOutsideColor()
		local arrBlocksAll =  nil
		local arrBlocks = {}
		
		--print("\niBlockQuantity")
		--print(  iBlockQuantity  )
		

		
		for i = 1, iBlockQuantity do 
		
			local _block = block:new()
			_block:init( display.contentWidth*.5, 0, iBlockLength, _blockType:GetInsideColor(), strBlockColorOutside )
	
			_block:insertInDisplayGroup( self.objDisplayGroup )		
			sharedData:BlockInsert( _block )
			
			arrBlocks[ #arrBlocks + 1 ] = _block
			 
		end
		 
		--print("\narrBlocks[ #arrBlocks ]")
		--print( arrBlocks[ #arrBlocks ] )
						
		_piece = piece:new( arrBlocks, _pieceType:GetBlockSize() )
		_piece:generateRandom()
		-- _piece:insertInDisplayGroup( self.objDisplayGroup )
		
		_piece:setBodyType("dynamic")
		
		sharedData:PieceInsert( _piece )
	end	
	
	local timerPieceSpawn = timer.performWithDelay( self.level.dropdelay, spawnPiece, 0 )

end

function physics_layer_gameplay:initialize_physics()
	self.physics = require("physics")
	self.physics.start( true ) -- prevent all bodies from sleeping

end

function physics_layer_gameplay:initialize_physics_walls()
	local rectFloor       = display.newRect( 0, (display.contentHeight - 50), display.contentWidth, 50 ) 
    local rectColumnLeft  = display.newRect( 0, 0, 45, display.contentHeight )
    local rectColumnRight = display.newRect( (display.contentWidth - 45), 0, 45, display.contentHeight )  
     
 	rectFloor.id = "wall"
    rectColumnLeft.id = "wall"
    rectColumnRight.id = "wall"
     	
	self.physics.addBody( rectFloor, "static", { friction=0.3, bounce=0 } )
	self.physics.addBody( rectColumnLeft, "static", { friction=0.3, bounce=0 } )
	self.physics.addBody( rectColumnRight, "static", { friction=0.3, bounce=0 } )	
end



