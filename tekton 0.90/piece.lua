require ("middleclass")
require ("block")


local physics = require "physics"

piece = class('piece')

function piece:initialize( arrBlocks, iLength )
	self.id        = "piece" 
	self.arrBlocks = arrBlocks
	self.iLength   = iLength 
	--self.displayGroup = display.newGroup()
	
	--physics.addBody( self.displayGroup, { density = 3.0, friction = 1.0, bounce = 0.1 } )
 
end



function piece:generateRandom()
	local function startDrag( event )
		local t = event.target
	
		local phase = event.phase
		if "began" == phase then
			display.getCurrentStage():setFocus( t )
			t.isFocus = true
	
			-- Store initial position
			t.x0 = event.x - t.x
			t.y0 = event.y - t.y
			
			-- Make body type temporarily "kinematic" (to avoid gravitional forces)
			event.target.bodyType = "kinematic"
			
			-- Stop current motion, if any
			event.target:setLinearVelocity( 0, 0 )
			event.target.angularVelocity = 0
	
		elseif t.isFocus then
			if "moved" == phase then
				t.x = event.x - t.x0
				t.y = event.y - t.y0
	
			elseif "ended" == phase or "cancelled" == phase then
				display.getCurrentStage():setFocus( nil )
				t.isFocus = false
				
				-- Switch body type back to "dynamic", unless we've marked this sprite as a platform
				--if ( not event.target.isPlatform ) then
				--	event.target.bodyType = "dynamic"
				--end
				event.target.bodyType = "dynamic"
	
			end
		end
	
		-- Stop further propagation of touch event!
		return true
	end	
	--[[
	--local iRemainingBlockQty = #self.arrBlocks
	--]]
	local aPoint = { x=0, y=0 }
	local points = {}
	local iXAdjacency  = 0
	local iYAdjacency  = 0
	local iPointRandom = 1
	local iPointIndex  = 0
	
	--print( "\n #self.arrBlocks "..#self.arrBlocks )
	
	points[1] = aPoint
	
	
	local iXMin = 999
	local iXMax = -999
	local iYMin = 999
	local iYMax = -999
	
	if #self.arrBlocks > 1 then
	
		for iPointIndex = 1, (#self.arrBlocks-1), 1 do

			repeat
				bIsNewPointOnList = false
				-- 1) Select 1 point
				iPointRandomIndex = math.random( 1, #points )
				--print("\n Random point index:".. iPointRandomIndex)
				
				-- 2)Create Adjacency Point
				if( math.random( 0, 1 ) > 0 ) then
					iXAdjacency = 0
					iYAdjacency = 1
					
					if( math.random( 0, 1 ) > 0 ) then
						iYAdjacency = -1
					end
				else
					iXAdjacency	 = 1
					iYAdjacency = 0
					
					if( math.random( 0, 1 ) > 0 ) then
						iXAdjacency = -1
					end					
				end
				
				-- 3) Check for adjacency
				for iPointCheckIndex = 1, #points, 1 do

						if(  ( points[iPointCheckIndex].x == (points[iPointRandomIndex].x + iXAdjacency) ) and
							 ( points[iPointCheckIndex].y == (points[iPointRandomIndex].y + iYAdjacency) )      ) then
							--print("\n On the list")
							--print("\n points[iPointCheckIndex].x:".. points[iPointCheckIndex].x)
							--print("\n points[iPointCheckIndex].y:".. points[iPointCheckIndex].y)
														
							bIsNewPointOnList = true
							
							break
						end

				end
					
				-- 4) If slot available...
				if( bIsNewPointOnList == false ) then
					local newPoint = {  x=(points[iPointRandomIndex].x + iXAdjacency),
									    y=(points[iPointRandomIndex].y + iYAdjacency)  }
									    
					--print("\n newPoint.x:".. newPoint.x )
					--print("\n newPoint.y:".. newPoint.y )
																    
					points[#points+1] = newPoint

				end

			until bIsNewPointOnList == false
			
		end
	end
	
	for iPointIndex = 1, #points, 1 do
		if( points[iPointIndex].x < iXMin ) then
			iXMin = points[iPointIndex].x
		end
			
		if( points[iPointIndex].x > iXMax ) then					
			iXMax = points[iPointIndex].x
		end
					
		if( points[iPointIndex].y < iYMin ) then					
			iYMin = points[iPointIndex].y
		end
					
		if( points[iPointIndex].y > iYMax ) then					
			iYMax =  points[iPointIndex].y
		end
	end
	
	local iPieceWidth  = math.abs( iXMin ) + math.abs( iXMax ) + 1
	local iPieceHeight = math.abs( iYMin ) + math.abs( iYMax ) + 1
	local iDiameter    = 0
	
	--print("\n MinMax")
	--print("\n Min (".. iXMin..","..iYMin..")")
	--print("\n Max (".. iXMax..","..iYMax..")")
	
	--print("\n WidthHeight")
	--print("\n iPieceWidth  = ".. iPieceWidth  )
	--print("\n iPieceHeight = ".. iPieceHeight )
	
	if( iPieceWidth >= iPieceHeight ) then
		 iRadius = iPieceWidth * self.iLength
	else
		 iRadius = iPieceHeight * self.iLength	
	end
	
	--  (radius) & (display.contentWidth - radius)
	local iBasePosition_x  = math.random(  iRadius,  (display.contentWidth-iRadius)  )
	local iBasePosition_y  = 0 - (display.contentHeight * .1)
	local iFinalPosition_x = 0
	local iFinalPosition_y = 0
		
	for iPointIndex = 1, #points, 1 do
		--self.displayGroup:insert( self.arrBlocks[iPointIndex]:getImageOutside() )
		--self.displayGroup:insert( self.arrBlocks[iPointIndex]:getImageInside() )
		self.arrBlocks[iPointIndex]:getImageInside():addEventListener( "touch", startDrag )	
		self.arrBlocks[iPointIndex]:getImageOutside():addEventListener( "touch", startDrag )	
		
		--print("\n Random point:".. iPointIndex)
		
		iFinalPosition_x = iBasePosition_x + (points[iPointIndex].x * self.iLength ) 
		iFinalPosition_y = iBasePosition_y - (points[iPointIndex].y * self.iLength )

		self.arrBlocks[iPointIndex]:position( iFinalPosition_x, iFinalPosition_y )
		self.arrBlocks[iPointIndex]:enablePhysics()
		--print("\n x:".. self.arrBlocks[iPointIndex]:getImageOutside().x .." y:"..self.arrBlocks[iPointIndex]:getImageOutside().y)

		--print( "\n x:".. self.arrBlocks[iPointIndex]:getImage().x .." y:"..self.arrBlocks[iPointIndex]:getImage().y)		
		--[[
        physics.newJoint( "weld", self.arrBlocks[iPointIndex], self.DisplayGroup, 0, 0 )
		--]]
		--[[
		physics.newJoint( "weld", self.arrBlocks[iPointIndex]:getImage(), self.displayGroup, points[iPointIndex].x * self.iLength, points[iPointIndex].y * self.iLength )
		
		--self.displayGroup:insert( self.arrBlocks[iPointIndex]:getImage() )
		--self.arrBlocks[iPointIndex]
		--]]
		if( iPointIndex > 1 ) then
			physics.newJoint( "weld", self.arrBlocks[iPointIndex]:getImageOutside(), self.arrBlocks[iPointIndex-1]:getImageOutside(), self.arrBlocks[iPointIndex].x, self.arrBlocks[iPointIndex].y )			
			--physics.newJoint( "weld", self.arrBlocks[iPointIndex]:getImageInside(), self.arrBlocks[iPointIndex-1]:getImageInside(), self.arrBlocks[iPointIndex].x, self.arrBlocks[iPointIndex].y )
			
		end			
			
	end
	
	self.arrBlocks[1]:getImageOutside():applyTorque(  math.random(-360, 360 )  )

----------------------------------	



--[[	
	print( "\n #self.arrBlocks "..#self.arrBlocks )
	print( "\n #points"..#points )
	print( "\n#self.arrBlocks == #points"..#self.arrBlocks == #points )
--]]
	
		


	points = nil
	--[[
	--]]	
end

function piece:getBlocks()
	return self.arrBlocks
end

function piece:setBodyType( strBodyType )
	for iBlockIndex = 1, #self.arrBlocks, 1 do
		local aBlock = self.arrBlocks[iBlockIndex]
		
		aBlock:setBodyType( strBodyType )
	end
end