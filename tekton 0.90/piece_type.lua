require ("middleclass")
require ("stringutils")

piece_type = class( 'piece_type' )

function piece_type:initialize( strRangeBlockQuantity, strRangeBlockSize )
	self.blockquantity = strRangeBlockQuantity
	self.blocksize     = strRangeBlockSize

end

function piece_type:GetBlockQuantity()
	iNumber1, iNumber2 = GetNumbersInRange( self.blockquantity )
	return math.random( iNumber1, iNumber2 )
	
end

function piece_type:GetBlockSize()	
	iNumber1, iNumber2 = GetNumbersInRange( self.blocksize )
	return math.random( iNumber1, iNumber2 )
		
end