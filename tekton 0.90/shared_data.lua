require ("middleclass")
require ("stack")
require ("block")
require ("level")

shared_data = class( 'shared_data' )


function shared_data:initialize()
	self.imageColorBlack = nil
	self.imageColorBlue = nil
	self.imageColorCyan = nil
	self.imageColorGold = nil
	self.imageColorGray = nil
	self.imageColorGreen = nil
	self.imageColorOrange = nil
	self.imageColorPurple = nil
	self.imageColorRed = nil
	self.imageColorWhite = nil
	
	local pieces = {}
	self.Pieces = pieces
	
	
	local blocks = {}
	self.Blocks = blocks
	
	self.imageColorBlack  = display.newImage( "images/color_black.jpg"  )
	self.imageColorBlue   = display.newImage( "images/color_blue.jpg"   )
	self.imageColorCyan   = display.newImage( "images/color_cyan.jpg"   )		
	self.imageColorGold   = display.newImage( "images/color_gold.jpg"   )
	self.imageColorGray   = display.newImage( "images/color_gray.jpg"   )
	self.imageColorGreen  = display.newImage( "images/color_green.jpg"  )
	self.imageColorOrange = display.newImage( "images/color_orange.jpg" )		
	self.imageColorPurple = display.newImage( "images/color_purple.jpg" )
	self.imageColorRed    = display.newImage( "images/color_red.jpg"    )
	self.imageColorWhite  = display.newImage( "images/color_white.jpg"  )
	
	self.imageStudioLogo  = display.newImage( "images/studio_logo.png" )
		
	self.imageColorBlack.alpha = 0
	self.imageColorBlue.alpha = 0
	self.imageColorCyan.alpha = 0
	self.imageColorGold.alpha = 0
	self.imageColorGray.alpha = 0
	self.imageColorGreen.alpha = 0
	self.imageColorOrange.alpha = 0
	self.imageColorPurple.alpha = 0
	self.imageColorRed.alpha = 0
	self.imageColorWhite.alpha = 0
	
	self.audioBlockRumble = audio.loadStream( "audio/audioBlockRumble.wav" )
	
	self.level = nil
	
	local _stateMenu     = stateGame_menu:new()	
	local _stateIntro    = stateGame_intro:new()	
	local _stateOptions  = stateGame_options:new()
	local _stateGameplay = stateGame_gameplay:new()	
	
	self.stateMachine    = statemachine:new()
	
	self.stateMachine:insertState( _stateMenu )
	self.stateMachine:insertState( _stateIntro )
	self.stateMachine:insertState( _stateOptions )
	self.stateMachine:insertState( _stateGameplay )	
		
	local Seed = os.clock()
	math.randomseed(Seed)
end

--function shared_data

function shared_data:GetBlocks()
	return self.Blocks
end

function shared_data:PieceInsert( _piece )
	self.Pieces[#self.Pieces + 1] = _piece
end

function shared_data:LoadLevel( strLevelFileName )
	local level = level:new( strLevelFileName )
	self.level = level
end

function shared_data:BlockInsert( _block )
	self.Blocks[#self.Blocks + 1] = _block
	
	print("\n#self.Blocks"..#self.Blocks)
end
