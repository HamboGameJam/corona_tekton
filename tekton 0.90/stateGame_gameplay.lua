require ( "state" )

stateGame_gameplay = class('stateGame_gameplay', state )

function stateGame_gameplay:initialize( objStateMachine )
	state:initialize( "gameplay", objStateMachine )
		
end

function stateGame_gameplay:enter()
end

function stateGame_gameplay:exit()
end

function stateGame_gameplay:update( fTimeEllapsed )
end
