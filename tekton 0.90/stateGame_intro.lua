require ( "state" )

stateGame_intro = class('stateGame_intro', state )

function stateGame_intro:initialize( objStateMachine )
	
	state:initialize( "intro", objStateMachine )
	
	self.objDisplayGroup = display.newGroup()
	
	local objDisplayRectBackground = display.newRect( (display.contentWidth  * -.10),
													  (display.contentHeight * -.10),
													  display.contentWidth  + (display.contentWidth  * .20),
													  display.contentHeight + (display.contentHeight * .20)  )													  
	objDisplayRectBackground:setFillColor( white )	
	
	self.imageStudioLogo = display.newImageRect( "images/column_left.png", 36, intColumnHeight, true )
	self.imageStudioLogo.alpha = 0
	
	
	objDisplayGroup.insert( objDisplayRectBackground )	
	objDisplayGroup.insert( self.imageStudioLogo )
														  
end

function stateGame_intro:enter()
	
end

function stateGame_intro:exit()
end

function stateGame_intro:update( fTimeEllapsed )
end
                                                                                                                                                                                                                                                                                                                                                                                                                                                   