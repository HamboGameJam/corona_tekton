require ( "state" )

stateGame_menu = class('stateGame_menu', state )

function stateGame_menu:initialize( objStateMachine )

	state:initialize( "menu", objStateMachine )
		
end

function stateGame_menu:enter()
end

function stateGame_menu:exit()
end

function stateGame_menu:update( fTimeEllapsed )
end
