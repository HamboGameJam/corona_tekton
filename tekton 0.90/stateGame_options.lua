require ( "state" )

stateGame_options = class('stateGame_options', state )

function stateGame_options:initialize( objStateMachine )
	state:initialize( "options", objStateMachine )
end

function stateGame_options:enter()
end

function stateGame_options:exit()
end

function stateGame_options:update( fTimeEllapsed )
end
