require ( "state" )

stateGame_title = class('stateGame_title', state )

function stateGame_title:initialize( objStateMachine )

	state:initialize( "title", objStateMachine )
		
end

function stateGame_title:enter()
end

function stateGame_title:exit()
end

function stateGame_title:update( fTimeEllapsed )
end
