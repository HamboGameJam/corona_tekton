require ("middleclass")
require ("state")

statemachine = class('statemachine')

function statemachine:initialize()
	self.stateCurrent  = nil
	self.statePrevious = nil
	self.stateNext     = nil
	self.arrStates     = nil
end

function statemachine:changeState( strStateName )
	if( self.stateCurrent ~= nil ) then
		self.stateCurrent:exit()
		self.statePrevious = self.stateCurrent
	end
	
	self.stateCurrent = arrStates[strStateName]
	self.stateCurrent:enter()
end

	
end

function statemachine:changeStateObject( objState )
	if( self.stateCurrent ~= nil ) then
		self.stateCurrent:exit()
		self.statePrevious = self.stateCurrent
	end
	
	self.stateCurrent = objState
	self.stateCurrent:enter()
end

function statemachine:getCurrentState()
	return self.stateCurrent	
end

function statemachine:goToNextState()
	self:changeState( self.stateNext )
end

function statemachine:goToPreviousState()
	self:changeState( self.statePrevious )
end

function statemachine:insertState( objState )
	self.arrStates[ #self.arrStates + 1 ] = objState
end

function statemachine:setNextState( strStateName )
	return self.stateNext = self.arrStates[ strStateName ]
end

function statemachine:update( fTime )
	if( self.stateCurrent ~= nil ) then
		self.stateCurrent:update( fTime )
	end
end

