function GetNumbersInRange( strRange_Number1_Number2 )
	--print("\n strRange_Number1_Number2 -")
	--print( strRange_Number1_Number2 )
	iDashPosition = string.find( strRange_Number1_Number2, "-" )
	
	if( iDashPosition ~= nil ) then	
		iNumber1 = string.sub( strRange_Number1_Number2, 1, (iDashPosition-1) )
		iNumber2 = string.sub( strRange_Number1_Number2, (iDashPosition+1), string.len(strRange_Number1_Number2) )
	else
		iNumber1 = strRange_Number1_Number2
		iNumber2 = strRange_Number1_Number2		
	end
	
	return iNumber1, iNumber2 
end