require ( 'DataDumper' )

function dump(...)
  print(DataDumper(...), "\n---")
end

function DisplayGroupSetFillColor( objDisplayGroup, ... )
  for intDisplayGroupIndex = 1, objDisplayGroup.numChildren, 1 do
    (objDisplayGroup[intDisplayGroupIndex]):setFillColor(...);
  end
end

function DisplayGroupGetRadialGradient( arrRGBAColorOutside, arrRGBAColorInside, iRadius )
	local objDisplayGroupRadialGradient = display.newGroup() 
	
	for intConcentricIndex = iRadius, 1, -1 do
    	local circleRadialLayer = display.newCircle( iRadius, iRadius, intConcentricIndex )
    	
    	local iR = (   (  arrRGBAColorOutside.r * (      intConcentricIndex/iRadius  )  ) +
    	               (  arrRGBAColorInside.r  * ( 1 - (intConcentricIndex/iRadius) )  )    ) * 0.5 
    	local iG = (   (  arrRGBAColorOutside.g * (      intConcentricIndex/iRadius  )  ) +
    	               (  arrRGBAColorInside.g  * ( 1 - (intConcentricIndex/iRadius) )  )    ) * 0.5
    	local iB = (   (  arrRGBAColorOutside.b * (      intConcentricIndex/iRadius  )  ) +    	
    	               (  arrRGBAColorInside.b  * ( 1 - (intConcentricIndex/iRadius) )  )    ) * 0.5
     	local iA = (   (  arrRGBAColorOutside.a * (      intConcentricIndex/iRadius  )  ) +    	
    	               (  arrRGBAColorInside.a  * ( 1 - (intConcentricIndex/iRadius) )  )    ) * 0.5   	    	    	
    	circleRadialLayer:setFillColor( iR, iG, iB, iA )
    	
    	objDisplayGroupRadialGradient:insert( circleRadialLayer )
	end
	
	return objDisplayGroupRadialGradient
end

function DisplayGroupGetTilesFromImage( strObjectName, strImageFileName, intImageWidth, intImageHeight )
  -- returns a grid of 9 DisplayObjects composed of 3 rows and three columns
  -- the center of the group lies at the center of the screen 
  local objDisplay_lb = display.newImageRect( strImageFileName, intImageWidth, intImageHeight, true )
  local objDisplay_mb = display.newImageRect( strImageFileName, intImageWidth, intImageHeight, true )
  local objDisplay_rb = display.newImageRect( strImageFileName, intImageWidth, intImageHeight, true )
  local objDisplay_lm = display.newImageRect( strImageFileName, intImageWidth, intImageHeight, true )
  local objDisplay_mm = display.newImageRect( strImageFileName, intImageWidth, intImageHeight, true )
  local objDisplay_rm = display.newImageRect( strImageFileName, intImageWidth, intImageHeight, true )
  local objDisplay_lt = display.newImageRect( strImageFileName, intImageWidth, intImageHeight, true )
  local objDisplay_mt = display.newImageRect( strImageFileName, intImageWidth, intImageHeight, true )
  local objDisplay_rt = display.newImageRect( strImageFileName, intImageWidth, intImageHeight, true )

  --local pointMidScreen
  local pointMidScreen_x = display.contentWidth  * .5
  local pointMidScreen_y = display.contentHeight * .5
  --[[
  --local rectPlateTiles
  local rectPlateTiles_left   = pointMidScreen_x - ( (objDisplay_bl.width * 3) * 0.5 )  
  local rectPlateTiles_right  = rectPlateTiles_left + (objDisplay_bl.width * 3)
  local rectPlateTiles_top    = pointMidScreen_y - ( (objDisplay_bl.height * 3) * 0.5 )
  local rectPlateTiles_bottom = rectPlateTiles_top + (objDisplay_bl.height * 3)
  --]]
  		--[[
		print( "\n\nlocal rectPlateTiles pointMidScreen " .. strObjectName )
		print( "\nobjDisplay_bl.width "..DataDumper(objDisplay_bl.width) )
		print( "\nobjDisplay_bl.height "..DataDumper(objDisplay_bl.height) )		
		print( "\npointMidScreen_x "..DataDumper(pointMidScreen_x) )
		print( "\npointMidScreen_y "..DataDumper(pointMidScreen_y) )
		print( "\nrectPlateTiles_left   "..DataDumper(rectPlateTiles_left) )
		print( "\nrectPlateTiles_right  "..DataDumper(rectPlateTiles_right) )
		print( "\nrectPlateTiles_top    "..DataDumper(rectPlateTiles_top) )
		print( "\nrectPlateTiles_bottom "..DataDumper(rectPlateTiles_bottom) )
		print( "\n\nlocal rectPlateTiles pointMidScreen" )
		--]]

  objDisplay_mm.x             = pointMidScreen_x 
  objDisplay_mm.y             = pointMidScreen_y
  objDisplay_mm.name          = "mm"

  objDisplay_lm.x             = pointMidScreen_x - objDisplay_lm.width
  objDisplay_lm.y             = pointMidScreen_y
  objDisplay_lm.name          = "lm"
  
  objDisplay_rm.x             = pointMidScreen_x + objDisplay_rm.width 
  objDisplay_rm.y             = pointMidScreen_y
  objDisplay_rm.name          = "rm"

  objDisplay_lt.x             = pointMidScreen_x - objDisplay_lt.width
  objDisplay_lt.y             = pointMidScreen_y - objDisplay_lt.height
  objDisplay_lt.name          = "lt"
       
  objDisplay_mt.x             = pointMidScreen_x
  objDisplay_mt.y             = pointMidScreen_y - objDisplay_mt.height
  objDisplay_mt.name          = "mt"
  
  objDisplay_rt.x             = pointMidScreen_x + objDisplay_rt.width
  objDisplay_rt.y             = pointMidScreen_y + objDisplay_rt.height
  objDisplay_rt.name          = "rt"
  
  
  objDisplay_mb.x             = pointMidScreen_x
  objDisplay_mb.y             = pointMidScreen_y + objDisplay_mb.height
  objDisplay_mb.name          = "mb"
  
  objDisplay_lb.x             = pointMidScreen_x - objDisplay_lb.width
  objDisplay_lb.y             = pointMidScreen_y + objDisplay_lb.height
  objDisplay_lb.name          = "lb"
  
  objDisplay_rb.x             = pointMidScreen_x + objDisplay_rb.width 
  objDisplay_rb.y             = pointMidScreen_y + objDisplay_rb.height
  objDisplay_rb.name          = "rb"    


  

  local objDisplayGroupTiled = display.newGroup()
  
  objDisplayGroupTiled:insert(objDisplay_lm)
  objDisplayGroupTiled:insert(objDisplay_mm)
  objDisplayGroupTiled:insert(objDisplay_rm)
  objDisplayGroupTiled:insert(objDisplay_lt)
  objDisplayGroupTiled:insert(objDisplay_mt)
  objDisplayGroupTiled:insert(objDisplay_rt)
  objDisplayGroupTiled:insert(objDisplay_lb)
  objDisplayGroupTiled:insert(objDisplay_mb)
  objDisplayGroupTiled:insert(objDisplay_rb)
  

  
  --objDisplayGroupTiled.xReference = objDisplayGroupTiled.width * 0.5
  --objDisplayGroupTiled.yReference = objDisplayGroupTiled.height * 0.5 
  
  objDisplayGroupTiled.x = rectPlateTiles_left --* 0.5--objDisplayGroupTiled.width * 0.5--display.contentWidth-
  objDisplayGroupTiled.y = rectPlateTiles_top --* 0.5--objDisplayGroupTiled.height * 0.5 --display.contentHeight-
  
  		--[[
		print( "\n-objDisplayGroupTiled" )

		print( "\nobjDisplayGroupTiled.x "..DataDumper(objDisplayGroupTiled.x) )
		print( "\nobjDisplayGroupTiled.y "..DataDumper(objDisplayGroupTiled.y) )
		print( "\nobjDisplayGroupTiled.xReference "..DataDumper(objDisplayGroupTiled.xReference) )
		print( "\nobjDisplayGroupTiled.yReference "..DataDumper(objDisplayGroupTiled.yReference) )
		print( "\nobjDisplayGroupTiled.width  "..DataDumper(objDisplayGroupTiled.width) )
		print( "\nobjDisplayGroupTiled.height "..DataDumper(objDisplayGroupTiled.height) )
		print( "\n-objDisplayGroupTiled" )
		--]]
		
  objDisplayGroupTiled.name = strObjectName
  
  return objDisplayGroupTiled

end